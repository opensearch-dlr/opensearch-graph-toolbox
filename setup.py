from setuptools import setup

setup(
    name='opensearch-graph-toolbox',
    version='0.9.1',
    url='',
    license='',
    author='Norman Nabhan',
    author_email='norman.nabhan@dlr.de',
    description='',
    packages=["opensearch_graph_toolbox"],
    install_requires=['clean-text', 'langdetect' ,'Cython', 'email-validator', 'geocoder', 'loguru', 'owlready2', 'pydantic',
                      'pygeohash', 'python-arango', 'Unidecode', 'tqdm']
)

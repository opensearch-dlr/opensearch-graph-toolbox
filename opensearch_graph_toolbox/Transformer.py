from loguru import logger
from owlready2 import *
from cleantext import clean
from langdetect import detect, LangDetectException
from pydantic import BaseModel

from .SchemeMeta import SchemeMeta


class Transformer:
    @staticmethod
    def run(obj):  # obj = base model
        if not isinstance(obj, (SchemeMeta, BaseModel)):
            logger.critical(f"{obj}")
        owl_class: type = obj.ressource
        # logger.debug(f"obj=<{obj}> owl_class=<{owl_class}>")
        kwargs = {}
        for vertex, v in obj.maps.items():
            if not v:
                continue
            # Cleaning for vertex name
            try:
                lang_code = detect(str(vertex))
                if lang_code != "de" and lang_code != "eng":
                    lang_code = "eng"  # fall back to eng
            except LangDetectException:
                lang_code = "eng"  # fall back to eng
            vertex_name = clean(vertex, no_line_breaks=True, lang=lang_code).split(".")[1]
            logger.debug(f"[{vertex_name}]vertex type {vertex.is_a} for {type(v)}")
            # cleaning for value of vertex
            if isinstance(v, str):
                try:
                    lang_code = detect(str(v))
                    if lang_code != "de" and lang_code != "eng":
                        lang_code = "eng"  # fall back to eng
                except LangDetectException:
                    lang_code = "eng"  # fall back to eng
                value = clean(v, no_line_breaks=True, lang=lang_code)
            else:
                value = v
            logger.debug(f"{vertex_name}=>{value}")
            if owlready2.FunctionalProperty in vertex.is_a:
                if owlready2.ObjectProperty in vertex.is_a:
                    logger.debug(f"{10 * '='}Start recursion for {vertex}")
                    kwargs[vertex_name] = Transformer.run(v)
                    check = True
                else:
                    kwargs[vertex_name] = value
                    check = True
            else:
                if owlready2.ObjectProperty in vertex.is_a:
                    logger.debug(f"{10 * '='}Start recursion for {vertex}")
                    kwargs[vertex_name] = [Transformer.run(cls) for cls in v]
                    check = True
                else:
                    kwargs[vertex_name] = [value]
                    check = True
            if not check:
                logger.critical("Error")
                raise ValueError()
        try:
            logger.debug(f"<{owl_class}> {obj.index}==>{kwargs}")
            logger.debug(f"{10 * '='}")
            return owl_class(obj.index, **kwargs)
        except (ValueError, TypeError) as e:
            logger.critical(obj)
            raise e

import json
from pathlib import Path
from tempfile import TemporaryDirectory

from rich import print

import typer
from rich.progress import track
from tqdm import tqdm
from datamodel_code_generator import InputFileType, generate

app = typer.Typer()


@app.command()
def e():
    ...


@app.command()
def generate_model(path: Path = typer.Argument(
    ...,
    exists=True,
    file_okay=True,
    dir_okay=True,
    writable=False,
    readable=True,
    resolve_path=True,
), destination: Path = typer.Argument(
    ..., exists=False, file_okay=True, dir_okay=False, writable=True,
    resolve_path=True
), model_name: str = typer.Argument(...)):
    from genson import SchemaBuilder
    builder = SchemaBuilder()
    if path.is_dir():
        files = list(Path(path).glob("*.json"))
        for json_file in track(files):
            with open(json_file, "r", encoding="utf8") as f:
                builder.add_object(json.load(f))

    elif path.is_file():
        with open(path, "r", encoding="utf8") as f:
            builder.add_object(json.load(f))

    generate(
        json.dumps(builder.to_schema()),
        input_file_type=InputFileType.JsonSchema,
        class_name=model_name,
        output=destination
    )
    print(f"Model generated")


if __name__ == "__main__":
    app()

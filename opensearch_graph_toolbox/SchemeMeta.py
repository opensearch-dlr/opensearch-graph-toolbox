from abc import ABC, abstractmethod

from owlready2 import Thing


class SchemeMeta(ABC):
    ressource: Thing = None

    @classmethod
    @abstractmethod
    def maps(cls):
        ...

    @property
    @abstractmethod
    def index(self):
        ...

    @property
    def corpus(self):
        return "UNKNOWN"

from owlready2 import *

onto = get_ontology(r"file://onto.owl")

with onto:
    """ Some general Attributes a 'Thing' can have """


    class Corpus(Thing):
        ...


    class Description(Thing):
        ...


    class Name(Thing):
        ...


    class URL(Thing):
        ...


    class has_corpus(Thing >> Corpus, FunctionalProperty):
        ...


    class has_description(Thing >> Description, FunctionalProperty):
        ...


    class has_name(Thing >> Name, FunctionalProperty):
        ...


    class has_url(Thing >> URL):
        ...


    """ === Date class and its attributes  """


    class DateStamp(Thing):
        ...


    class has_year(DateStamp >> int, FunctionalProperty):
        ...


    class has_month(DateStamp >> int, FunctionalProperty):
        ...


    class has_day(DateStamp >> int, FunctionalProperty):
        ...


    class has_datestamp(Thing >> DateStamp, FunctionalProperty):
        ...


    """ === Geo class and its attributes  """


    class GeoCoordinates(Thing):
        pass


    class has_lat(GeoCoordinates >> float, FunctionalProperty):
        pass


    class has_lng(GeoCoordinates >> float, FunctionalProperty):
        pass


    class Quality(Thing):
        pass


    class Location(Thing):
        pass


    class has_quality(Location >> Quality, FunctionalProperty):  # like PointAddress or POI
        pass


    class has_address(Location >> str, FunctionalProperty):
        pass


    class has_geo_coordinates(Location >> GeoCoordinates, FunctionalProperty):
        pass


    class Person(Thing):
        pass


    class has_first_name(Person >> str, FunctionalProperty):
        pass


    class has_last_name(Person >> str, FunctionalProperty):
        pass


    class has_email(Person >> str):
        pass


    class has_honorific(Person >> str):
        pass


    class has_email_address(Person >> str):
        pass


    class has_association(Person >> Location):
        pass


    class has_orcid(Person >> str, FunctionalProperty):
        pass


    class Author(Person):
        equivalent_to = [Person & has_orcid.min(1, str)]


    """ === Document class and its attributes  """


    class Document(Thing):
        pass


    class has_abstract(Document >> str, FunctionalProperty):
        pass


    class has_author(Document >> Person):
        pass


    class has_date_created(Document >> str, FunctionalProperty):
        pass


    class has_keywords(Document >> str, FunctionalProperty):
        pass


    class has_location(Document >> str, FunctionalProperty):
        pass





    class DlrSchwerpunkt(Thing):
        pass



    class has_dlr_schwerpunkt(Document >> DlrSchwerpunkt, FunctionalProperty):
        pass


    class has_dlr_project(Document >> str, FunctionalProperty):
        pass


    class has_type(Document >> str, FunctionalProperty):
        pass

    class has_elib_id(Document >> str, FunctionalProperty):
        pass


    """ === GitRepository class and its attributes  """


    class GitRepository(Thing):
        ...


    class has_repository_id(GitRepository >> int, FunctionalProperty):
        ...


    class has_total_commits(GitRepository >> int, FunctionalProperty):
        ...


    class has_created_at(GitRepository >> str, FunctionalProperty):
        ...


    class has_last_activity_at(GitRepository >> str, FunctionalProperty):
        ...


    class has_languages(GitRepository >> str):
        ...


    class has_readme(GitRepository >> str, FunctionalProperty):
        ...


    class has_readmeclassifier(GitRepository >> str, FunctionalProperty):
        ...


    class has_members(GitRepository >> Person):
        ...


    class has_contributors(GitRepository >> Person):
        ...


    class Readmeclassifier(Thing):
        ...


    class has_heading_markdown(Readmeclassifier >> str, FunctionalProperty):
        ...


    class has_section_code(Readmeclassifier >> str, FunctionalProperty):
        ...

# generated by datamodel-codegen:
#   filename:  ns.json
#   timestamp: 2022-05-19T10:26:51+00:00

"""
The definition of the data types may look odd in some places. But these pyndatic models were generated on the basis
of more than 123.000 publications. So these models represent how the dataset of elib looks like
and not how it has to look optimal! So the normalization still has to be done.
"""

from __future__ import annotations

import re
from abc import ABC
from datetime import datetime
from enum import Enum, EnumMeta
from typing import Any, List, Optional, Union
from urllib.parse import urlparse

import six
from cleantext import clean
from email_validator import validate_email, EmailNotValidError
from loguru import logger
from owlready2 import Thing
from pydantic import AnyUrl, Field, AnyHttpUrl
from pydantic import BaseModel

from .SchemeMeta import SchemeMeta
from .geocode_model import GeoCode
from .opensearch_ontology import Document as OWLDocument, has_type
from .opensearch_ontology import has_last_name, has_first_name, has_honorific, onto, Person, has_orcid, \
    has_abstract, has_author, has_date_created, has_keywords, has_location, has_elib_id, has_email_address, \
    has_geo_coordinates


class DlrSchwerpunktEnum(Enum):
    D001 = "D001"
    _undef = "_undef"
    astronautics = "astronautics"
    aviation = "aviation"
    energy = "energy"
    other = "other"
    space = "space"
    transport = "transport"


class DateType(Enum):
    lido = "lido"
    published = "published"
    submitted = "submitted"
    event = "event"
    lastmod = "lastmod"
    datestamp = "datestamp"


class FullTextStatus(Enum):
    restricted = "restricted"
    none = "none"
    public = "public"


class Ispublished(Enum):
    inpress = "inpress"
    unpub = "unpub"
    submitted = "submitted"
    accepted = "accepted"
    pub = "pub"


class MetadataVisibility(Enum):
    show = "show"
    no_search = "no_search"


class MonographType(Enum):
    dlr_internal_report = "dlr_internal_report"
    other = "other"
    dlr_report = "dlr_report"
    dlr_research_report = "dlr_research_report"
    project_report = "project_report"


class EprintStatus(Enum):
    archive = "archive"




class EventType(Enum):
    conference_n = "conference_n"
    conference = "conference"
    other = "other"
    conference_i = "conference_i"
    workshop = "workshop"


class PresType(Enum):
    speech = "speech"
    paper = "paper"
    other = "other"
    lecture = "lecture"
    keynote = "keynote"
    poster = "poster"


class ThesisType(Enum):
    other = "other"
    habilitation = "habilitation"
    thesis = "thesis"
    student = "student"
    masters = "masters"
    magister = "magister"
    project = "project"
    bachelor = "bachelor"
    diploma = "diploma"


class Fp7Type(Enum):
    info_eu_repo_semantics_article = "info:eu-repo/semantics/article"
    info_eu_repo_semantics_book = "info:eu-repo/semantics/book"
    info_eu_repo_semantics_bookPart = "info:eu-repo/semantics/bookPart"
    info_eu_repo_semantics_other = "info:eu-repo/semantics/other"
    info_eu_repo_semantics_conferenceObject = "info:eu-repo/semantics/conferenceObject"


class PatentOffice(Enum):
    Europaeisches_Patentamt = "Europäisches Patentamt"
    Deutsches_Patent__und_Markenamt = "Deutsches Patent- und Markenamt"


class PatentApplicant(Enum):
    DLR_e_V_ = "DLR e.V."
    Deutsches_Zentrum_fuer_Luft__und_Raumfahrt_e_V_ = (
        "Deutsches Zentrum für Luft- und Raumfahrt e.V."
    )


class AccessRights(Enum):
    info_eu_repo_semantics_closedAccess = "info:eu-repo/semantics/closedAccess"
    info_eu_repo_semantics_embargoedAccess = "info:eu-repo/semantics/embargoedAccess"
    info_eu_repo_semantics_openAccess = "info:eu-repo/semantics/openAccess"
    info_eu_repo_semantics_restrictedAccess = "info:eu-repo/semantics/restrictedAccess"


class ImportPlugin(Enum):
    EPrints__Plugin__Import__XML = "EPrints::Plugin::Import::XML"
    EPrints__Plugin__Import__OpenXML = "EPrints::Plugin::Import::OpenXML"
    EPrints__Plugin__Import__BibTeX = "EPrints::Plugin::Import::BibTeX"
    EPrints__Plugin__Import__PDF = "EPrints::Plugin::Import::PDF"
    EPrints__Plugin__Import__DOI = "EPrints::Plugin::Import::DOI"


class Name(BaseModel, SchemeMeta):

    @property
    def corpus(self):
        return "DLR elib"

    honourific: Optional[str] = Field("", title="Honourific")
    lineage: Optional[Any] = Field("", title="Lineage")
    given: Optional[str] = Field("", title="Given")
    family: Union[int, str] = Field(..., title="Family")

    ressource: Thing = Person

    @staticmethod
    def clean_names(name: Union[str, None]):
        if name:
            return clean(re.sub(r"\s+", "", name, flags=re.UNICODE)).replace('"', "")
        return None

    @property
    def maps(self):
        return {
            has_last_name: self.clean_names(self.family),
            has_first_name: self.clean_names(self.given),
            has_honorific: self.clean_names(self.honourific)
        }

    @property
    def index(self):
        ret = []
        if self.honourific:
            ret.append(self.clean_names(self.honourific))
        if self.given:
            ret.append(self.clean_names(self.given))
        ret.append(self.clean_names(self.family))
        return clean("-".join(ret))


class Creator(BaseModel, SchemeMeta):
    name: Name
    id: Optional[Union[int, str]] = Field(None, title="Id")
    orcid: Optional[Union[AnyHttpUrl, str]] = Field(None, title="Orcid")

    ressource = Person

    @property
    def corpus(self):
        return "DLR elib"

    def transform_orcid(self):
        if self.orcid is None:
            return None
        elif isinstance(self.orcid, AnyHttpUrl):
            logger.debug(f"{self.name.index} has ORCID")
            # case like https://orcid.org/0000-0003-2287-2910
            domain = urlparse(self.orcid)
            if domain.hostname == 'orcid.org':
                return (domain.path[1:]).replace("-", "")

    def transform_id(self, id):
        if id is None or isinstance(id, int):
            return None
        try:
            email = validate_email(id, check_deliverability=False).email.lower()
            logger.debug(f"{self.name.index} has Email {email}")
            return email
        except EmailNotValidError as e:
            # email is not valid
            pass
        """geocoded = geocoder.arcgis(clean(id))
        if geocoded.ok:
            logger.debug(f"{self.name.index} has Location")
            return GeoCode(**geocoded.json)
        """
        return None

    @property
    def maps(self):
        kwargs = {}
        process_id_field = self.transform_id(self.id)
        if isinstance(process_id_field, str):
            kwargs.update({has_email_address: process_id_field})
        elif isinstance(process_id_field, GeoCode):
            kwargs.update({has_geo_coordinates: process_id_field})

        kwargs.update(
            {
                has_last_name: Name.clean_names(self.name.family),
                has_first_name: Name.clean_names(self.name.given),
                has_honorific: Name.clean_names(self.name.honourific),
                has_orcid: self.transform_orcid(),
            }
        )
        return kwargs

    @property
    def index(self):
        ret = []
        if self.name.honourific:
            ret.append(Name.clean_names(self.name.honourific))
        if self.name.given:
            ret.append(Name.clean_names(self.name.given))
        ret.append(Name.clean_names(self.name.family))
        return "-".join(ret)


class Name1(BaseModel, SchemeMeta):
    ressource = Person

    @property
    def corpus(self):
        return "DLR elib"

    @property
    def maps(self):
        return {
            has_last_name: Name.clean_names(self.family),
            has_first_name: Name.clean_names(self.given),
            has_honorific: Name.clean_names(self.honourific)
        }

    @property
    def index(self):
        ret = []
        if self.honourific:
            ret.append(Name.clean_names(self.honourific))
        if self.given:
            ret.append(Name.clean_names(self.given))
        if self.family:
            ret.append(Name.clean_names(self.family))
            return "-".join(ret)
        return "Unknown"

    honourific: Optional[Any] = Field(None, title="Honourific")
    family: Optional[str] = Field(None, title="Family")
    lineage: Optional[Any] = Field(None, title="Lineage")
    given: Optional[str] = Field(None, title="Given")


class Editor(BaseModel):
    name: Optional[Name1] = None
    id: Optional[str] = Field(None, title="Id")
    orcid: Optional[str] = Field(None, title="Orcid")


class RelationItem(BaseModel):
    type: str = Field(..., title="Type")
    uri: str = Field(..., title="Uri")


class File(BaseModel):
    filesize: int = Field(..., title="Filesize")
    filename: Union[int, str] = Field(..., title="Filename")
    mtime: str = Field(..., title="Mtime")
    objectid: int = Field(..., title="Objectid")
    uri: str = Field(..., title="Uri")
    datasetid: str = Field(..., title="Datasetid")
    mime_type: str = Field(..., title="Mime Type")
    fileid: int = Field(..., title="Fileid")
    hash: Optional[str] = Field(None, title="Hash")
    hash_type: Optional[str] = Field(None, title="Hash Type")


class Document(BaseModel):
    docid: int = Field(..., title="Docid")
    pos: int = Field(..., title="Pos")
    language: str = Field(..., title="Language")
    rev_number: int = Field(..., title="Rev Number")
    security: str = Field(..., title="Security")
    relation: Optional[List[RelationItem]] = Field(None, title="Relation")
    main: Optional[Union[int, str]] = Field(None, title="Main")
    uri: str = Field(..., title="Uri")
    mime_type: Optional[str] = Field(None, title="Mime Type")
    eprintid: int = Field(..., title="Eprintid")
    files: Optional[List[File]] = Field(None, title="Files")
    format: str = Field(..., title="Format")
    placement: Optional[int] = Field(None, title="Placement")
    ispublished: Optional[str] = Field(None, title="Ispublished")
    formatdesc: Optional[str] = Field(None, title="Formatdesc")
    license: Optional[str] = Field(None, title="License")
    date_embargo: Optional[Union[int, str]] = Field(None, title="Date Embargo")


class Funder(BaseModel):
    project_id: Optional[Union[int, str]] = Field(None, title="Project Id")
    funder: Optional[str] = Field(None, title="Funder")


class Name2(BaseModel):
    honourific: Optional[Any] = Field(None, title="Honourific")
    lineage: Optional[Any] = Field(None, title="Lineage")
    family: str = Field(..., title="Family")
    given: str = Field(..., title="Given")


class CorrCreator(BaseModel):
    id: Optional[str] = Field(None, title="Id")
    orcid: Optional[str] = Field(None, title="Orcid")
    name: Name2


class ElibModel(BaseModel, SchemeMeta):
    ressource = OWLDocument

    @property
    def corpus(self):
        return "DLR elib"

    @property
    def maps(self):
        return {
            has_abstract: clean(self.abstract),
            has_author: self.creators,
            has_date_created: str(self.date),
            has_keywords: clean(self.keywords),
            has_location: clean(self.event_location),
            has_elib_id: self.eprintid,
            has_type: self.type
        }

    @property
    def index(self):
        return f"elib-{self.eprintid}"

    rev_number: int = Field(..., title="Rev Number")
    dlr_schwerpunkt: Optional[List[DlrSchwerpunktEnum]] = Field(
        None, title="Dlr Schwerpunkt"
    )  # like  'R289', 'L086'
    dlr_project: Optional[List[str]] = Field(None, title="Dlr Project")
    approvedby: Optional[int] = Field(None, title="Approvedby")  # some ids?
    scopus: bool = Field(..., title="Scopus")
    hgf_forschungsbereich: Optional[List[str]] = Field(
        None, title="Hgf Forschungsbereich"
    )  # {'other', 'transport_space', 'lrv', '_error', 'energy'}
    monograph_id: Optional[Union[float, str]] = Field(
        None, title="Monograph Id"
    )  # Universität Stuttgart, 29.10.93', 'DLR-IB 435-2011/10'
    creators: Optional[List[Creator]] = Field(None, title="Creators")
    dlr_programm: Optional[List[str]] = Field(
        None, title="Dlr Programm"
    )  # 'vsm', 'lrr', 'D006', 'ltt'
    status_changed: Optional[datetime] = Field(None, title="Status Changed")
    lastmod: datetime = Field(..., title="Lastmod")
    dlr_project_search: Optional[List[str]] = Field(
        None, title="Dlr Project Search"
    )  # 'R563', 'W134', 'E011', 'R427'
    note: Optional[Union[int, str]] = Field(None, title="Note")  # comments
    eprintid: int = Field(..., title="Eprintid")
    date_type: DateType = Field(..., title="Date Type")
    uri: AnyUrl = Field(..., title="Uri")
    full_text_status: FullTextStatus = Field(..., title="Full Text Status")
    date_issue: Optional[Union[int, str]] = Field(
        None, title="Date Issue"
    )  # int means year and noisy datetimes
    lidoid: Optional[Union[int, str]] = Field(
        None, title="Lidoid"
    )  # 'AFE9519901771995', 'ANW9334703731993'
    pages: Optional[int] = Field(None, title="Pages")
    subjects: Optional[List[Union[int, str]]] = Field(
        None, title="Subjects"
    )  # like 'at-nm', 265, 'LY'
    location: Optional[List[str]] = Field(
        None, title="Location"
    )  # 'kp', 'la', 'bc' .. dlr location mnemonics?
    oa: Optional[bool] = Field(None, title="Oa")
    ispublished: Optional[Ispublished] = Field(None, title="Ispublished")
    userid: Optional[int] = Field(None, title="Userid")
    metadata_visibility: MetadataVisibility = Field(..., title="Metadata Visibility")
    isi: bool = Field(..., title="Isi")
    monograph_types: Optional[List[MonographType]] = Field(
        None, title="Monograph Types"
    )
    eprint_status: EprintStatus = Field(..., title="Eprint Status")
    hgf_programmthema: Optional[List[Union[int, str]]] = Field(
        None, title="Hgf Programmthema"
    )
    type: str = Field(..., title="Type")
    hgf_programm: Optional[List[Union[int, str]]] = Field(None, title="Hgf Programm")
    doaj: Optional[bool] = Field(None, title="Doaj")
    dir: str = Field(..., title="Dir")
    title: Optional[str] = Field(None, title="Title")
    date: Union[int, str] = Field(
        ..., title="Date"
    )  # int means year and noisy datetimes
    keywords: Optional[str] = Field(None, title="Keywords")
    datestamp: Optional[str] = Field(None, title="Datestamp")
    event_location: Optional[str] = Field(
        None, title="Event Location"
    )  # "real locations
    invited_lecture: Optional[bool] = Field(None, title="Invited Lecture")
    event_types: Optional[List[EventType]] = Field(None, title="Event Types")
    pres_types: Optional[List[PresType]] = Field(None, title="Pres Types")
    event_organizer: Optional[str] = Field(
        None, title="Event Organizer"
    )  # Institutions
    event_title: Optional[Union[int, str]] = Field(
        None, title="Event Title"
    )  # Event names
    abstract: Optional[str] = Field(None, title="Abstract")
    thesis_type: Optional[ThesisType] = Field(None, title="Thesis Type")
    volume: Optional[Union[float, str]] = Field(None, title="Volume")
    refereed: Optional[bool] = Field(None, title="Refereed")
    number: Optional[Union[float, str]] = Field(None, title="Number")
    publication: Optional[Union[int, str]] = Field(None, title="Publication")
    pagerange: Optional[Union[float, str]] = Field(None, title="Pagerange")
    event_dates: Optional[Union[float, str]] = Field(
        None, title="Event Dates"
    )  # weird stuff like Sommersemester 2011
    publication2: Optional[Union[int, str]] = Field(None, title="Publication2")
    publisher: Optional[str] = Field(None, title="Publisher")  # like John Wiley&Sons
    editors: Optional[List[Editor]] = Field(None, title="Editors")
    series: Optional[Union[float, str]] = Field(None, title="Series")
    isbn: Optional[Union[float, str]] = Field(None, title="Isbn")
    creators_ok: Optional[bool] = Field(None, title="Creators Ok")
    fp7_type: Optional[Fp7Type] = Field(None, title="Fp7 Type")
    institution: Optional[str] = Field(None, title="Institution")  # TH Köln
    official_url: Optional[AnyUrl] = Field(None, title="Official Url")
    doi: Optional[Union[float, str]] = Field(None, title="Doi")
    documents: Optional[List[Document]] = Field(None, title="Documents")
    department: Optional[Union[int, str]] = Field(
        None, title="Department"
    )  # Fakultät Maschinenbau/ Umwelttechnik
    ref_abstract: Optional[bool] = Field(None, title="Ref Abstract")
    ref_full: Optional[bool] = Field(None, title="Ref Full")
    hybrid: Optional[bool] = Field(None, title="Hybrid")
    fp7_project: Optional[bool] = Field(None, title="Fp7 Project")
    suggestions: Optional[Union[int, str]] = Field(
        None, title="Suggestions"
    )  # like a comment
    issn: Optional[Union[int, str]] = Field(
        None, title="Issn"
    )  # 1432-1114 (electronic Version)
    date_sub: Optional[Union[int, str]] = Field(
        None, title="Date Sub"
    )  # date field but really noisy
    data: Optional[str] = Field(None, title="Data")  # like a comment
    publicationid: Optional[int] = Field(None, title="Publicationid")  # some id ?
    topic_security: Optional[bool] = Field(None, title="Topic Security")
    industry_collab: Optional[bool] = Field(None, title="Industry Collab")
    date_last_exam: Optional[Union[int, str]] = Field(
        None, title="Date Last Exam"
    )  # date field but really noisy
    topic_digit: Optional[bool] = Field(None, title="Topic Digit")
    succeeds: Optional[int] = Field(None, title="Succeeds")  # some id ?
    patent_id: Optional[str] = Field(
        None, title="Patent Id"
    )  # like DE 10 2011 010 147 B3
    patent_office: Optional[PatentOffice] = Field(None, title="Patent Office")
    patent_applicant: Optional[PatentApplicant] = Field(None, title="Patent Applicant")
    funders: Optional[List[Funder]] = Field(None, title="Funders")
    access_rights: Optional[AccessRights] = Field(None, title="Access Rights")
    fp7_project_id: Optional[Union[int, str]] = Field(
        None, title="Fp7 Project Id"
    )  # stuff like '03EMEN18A', 881574, 'info:eu-repo/grantAgreement/EC/H2020/727476'
    id_number: Optional[str] = Field(
        None, title="Id Number"
    )  # ids like doi:10.1016/j.pss.2011.12.002
    book_title: Optional[str] = Field(
        None, title="Book Title"
    )  # like 'AIAA Aviation 2019 Forum'
    corr_creator: Optional[CorrCreator] = None
    place_of_pub: Optional[str] = Field(
        None, title="Place Of Pub"
    )  # like 'München', 'Virtual',
    import_plugin: Optional[ImportPlugin] = Field(None, title="Import Plugin")
